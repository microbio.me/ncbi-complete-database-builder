#!bin/bash

# dependencies
# sudo apt-get install parallel
# sudo apt-get install pigz
# sudo apt-get install ncbi-blast+

# run: bash ncbi-db-maker.sh viral/bacterial/fungi/archaea [complete]

tax_map="tax_map.txt"

genomes="genomes_$1_ftp.txt"
assembly_summary="assembly_summary.txt"

if [ $1 == "human" ]; then
  wget ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_mammalian/Homo_sapiens/assembly_summary.txt
else
  wget ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/$1/assembly_summary.txt
fi


if [ $2 == "complete" ]; then
	awk -F '\t' '{if($12=="Complete Genome"){
	  x=$1;
	  gsub(/ /,"_", x); 
	  y=$16;
	  gsub(/ /, "_", y); 
	  gsub(",", "", y); 
	  print $20 "/" x "_" y "_genomic.fna.gz"
	 }
	}' $assembly_summary > $genomes
else
	awk -F '\t' '{if($1 !~ /^#/){
	    x=$1; 
	    gsub(/ /,"_", x); 
	    y=$16; 
	    gsub(/ /, "_", y); 
	    gsub(",", "", y); 
	    print $20 "/" x "_" y "_genomic.fna.gz"
	  }
	}' $assembly_summary > $genomes 
fi


mv $assembly_summary "assembly_summary_$1.txt"
$assembly_summary = "assembly_summary_$1.txt"

mkdir $1

#old command
#cat $genomes | parallel -j 40 --gnu "wget -P $1 \"{}\""

cat $genomes | xargs -I{} -n1 -P8 wget --retry-connrefused -c -P $1 {}

echo "Genomes downloaded - decompressing files"

pigz -d -r $1

echo "Decompression completed - concatenating genomes"

cat $1/*.fna > all_$1.fasta

echo "Formatting headers"

# edit the tax id in the header to so that makeblastdb identifies it.
sed -i "s/^>\([^ ]*\) />ref|\1| /g" all_$1.fasta

mkdir $1_db

mv all_$1.fasta $1_db/all_$1.fasta

rm $assembly_summary
rm $genomes

echo "making DB with makeblastdb"

makeblastdb -dbtype 'nucl' -in $1_db/all_$1.fasta -taxid_map $tax_map -parse_seqids
