# Creating an updated NCBI index

This example uses the bacteria genome reference.

## 1. Running manually

### 1.1. Download the summary file: replace `bacteria` with required assembly.

`wget ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt`


### 1.2. Parse the addresses into a clean text file 

Complete genomes only:

```
awk -F '\t' '{if($12=="Complete Genome"){
	  x=$1;
	  gsub(/ /,"_", x); 
	  y=$16;
	  gsub(/ /, "_", y); 
	  gsub(",", "", y); 
	  print $20 "/" x "_" y "_genomic.fna.gz"
	 }
	}' assembly_summary_bacteria.txt > complete_genomes_bacteria_ftp.txt
```

All:

```
awk -F '\t' '{if($1 !~ /^#/){
	    x=$1; 
	    gsub(/ /,"_", x); 
	    y=$16; 
	    gsub(/ /, "_", y); 
	    gsub(",", "", y); 
	    print $20 "/" x "_" y "_genomic.fna.gz"
	  }
	}' assembly_summary_bacteria.txt > complete_genomes_bacteria_ftp.txt
```

After this - be careful that any comment lines haven't created empty lines.

**WARNING: If choosing all genomes from the bacteria refseq - this is very big. It weighs in at 160GB gzipped**

#### 1.2.1. If some of the files fail to download

**Don't** delete them all and start again.

Run the `ftp_check.sh` shell script. This will look for the missing files and attempt the downloads - They may be missing due to invalid file paths, or just bad connections:

`bash ftp_check.sh file_with_ftp_endpoints.txt directory_to_look_for_files`

Output to a file to check none are missed:

`bash ftp_check.sh complete_genomes_bacteria_ftp.txt bacteria > missed_bacteria_downloads.txt`


### 1.3. Make directory

`mkdir bacteria`

### 1.4. Fetch the data

**WARNING: this outputs a lot of stuff - use the `wget -q` flag to suppress the output if needed.**

`wget -P bacteria -i complete_genomes_bacteria_ftp.txt`

or parallel:

`cat complete_genomes_bacteria_ftp.txt | xargs -I{} -n1 -P8 wget --retry-connrefused -c -P $1 {}`


### 1.5. Extract

`pigz -d bacteria/*.gz`


### 1.6. Concat the data

`cat bacteria/*.fna > all_bacteria.fasta`

### 1.7. Format the IDs to pick up the tax ids: 

`sed -i "s/^>\([^ ]*\) />ref|\1| /g" all_bacteria.fasta`

### 1.8. Index the file

`makeblastdb -dbtype 'nucl' -in all_bacteria.fasta -taxid_map tax_map.txt -parse_seqids1`


# 2. Using the bash script

`bash ncbi-db-maker bacteria|viral|fungi|archaea|protozoa|human [complete]`

The `[complete]` parameter is optional and will decide which genomes are included (complete only or everything).

### 2.1. NB: If the initial wget loop failed

If you had to stop and finish off the downloads manually:

- finish the downloads with the `ftp_check.sh` script - (shown in section 1.2.1)
- run the `make_db.sh` script instead.

This continues the same pipeline from after the downloads have completed. Just add the directory as an argument

`bash make_db.sh directory` 

i.e 

`bash make_db.sh bacteria|viral|fungi|archaea|protozoa|human`



