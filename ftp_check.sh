#!bin/bash

# using this script: 
# bash ftp_check.sh ftp_file_with_paths directory

file=$1
directory=$2

while IFS= read -r line; do
 
if [ -f "$directory$(basename $line)" ]; then
     echo "$directory$(basename $line) was found"  
else
  echo "$directory$(basename $line) doesnt exist - trying download"
  wget --retry-connrefused -c -P $directory $line 

fi

	 
done < "$file"


