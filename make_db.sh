#!bin/bash

# dependencies
# sudo apt-get install parallel
# sudo apt-get install pigz
# sudo apt-get install ncbi-blast+

#Just the end of the pipe, in case it needed to be stopped + the failed files installed manually.

#run bash make_db.sh folder

tax_map="tax_map.txt"

pigz -d -r $1

echo "Decompression completed - concatenating genomes"

cat $1/*.fna > all_$1.fasta

echo "Formatting headers"

# edit the tax id in the header to so that makeblastdb identifies it.
sed -i "s/^>\([^ ]*\) />ref|\1| /g" all_$1.fasta

mkdir $1_db

mv all_$1.fasta $1_db/all_$1.fasta

echo "making DB with makeblastdb"

makeblastdb -dbtype 'nucl' -in $1_db/all_$1.fasta -taxid_map $tax_map -parse_seqids
